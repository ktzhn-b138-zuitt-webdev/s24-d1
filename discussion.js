// QUERY OPERATORS

// $gt (greater than) / $gte (greater than or equal to)
// SYNTAX: db.collectionName.find({ field: { $gt: value}});
// SYNTAX: db.collectionName.find({ field: { $gte: value}});


db.users.find({"age": {$gt: 50}});
db.users.find({"age": {$gt: 70}});
db.users.find({"age": {$gt: 90}});

// $t (less than) / $lte (less than or equal to)
// SYNTAX: db.collectionName.find({ field: { $lt: value}});
// SYNTAX: db.collectionName.find({ field: { $lte: value}});

// MINI ACTIVITY:
// Retrive all documents where age is less than 50.
db.users.find({"age": {$lt: 50}});
// .... where age is less than or equal to 50
db.users.find({"age": {$lte: 50}});

// $ne (not equal)
// SYNTAX: db.collectionName.find({ field: { $ne: value}});

// MINI ACTIVITY
// Retrive all documents where age is not equal to 82
db.users.find({"age": {$ne: 82}});

// $in
// SYNTAX:
// db.collectionName.find({ field: { $in: [value, ...]}});

// MINI ACTIVITY:

db.users.find({ "lastName": { $in: ["Hawking","Doe", "Gates"]}});

// Logical Query Operators

// $or
// SYNTAX: db.collectionName.find({$or:[{fieldA: valueA}, {fieldB: valueB}]})

// MINI ACTIVITY
// Retrieve all documents where firstName is "Neil" or age is 25
// Retrieve all documents where firstName is "Neil" or age is greater than 30
db.users.find({$or:[
	{"firstName": "Neil"}, 
	{"age": 25}
	]});
db.users.find({$or:[
	{"firstName": "Neil"}, 
	{"age": {$gt: 30}}
	]});


// $and
// SYNTAX: db.collectionNAme.find({$and:[{fieldA: valueA}, {fieldB: valueB}]})

// MINI ACTIVITY
// Retrieve all documents where age not eequal to 82 and age is not equal to 76
db.users.find({$and:[
	{"age": {$ne: 82}}, 
	{"age": {$ne: 76}}
	]});


// Field Projections

// Inclusion
// Fields are included to the resulting documents
// SYNTAX: 
// db.collectionName.find({field: value}, {field : 1});

// MINI ACTIVITY
// Retrieve all firstNames where firstName is not equal to "Jane".

// By default, _id is set to 1 in field projections

db.users.find(
	{"firstName": {$ne: "Jane"}}, 
	{"firstName" : 1}
	);

// Exclusion
// Fields are excluded to the resulting documents
// SYNTAX: 
// db.collectionName.find({field: value}, {field : 0, [field: 0, ...]});

// MINI ACTIVITY:
// Retrieve all documents where firstname is not equal to "John"
// Exclude the _id, contact and department fields to the resulting documents

db.users.find(
	{"firstName": {$ne: "John"}}, 
	{"_id": 0,
	 "contact":0,
	 "department": 0}
	);

// Inclusion/Exclusion of Nested Document Fields
// SYNTAX:
// db.collectionName.find({field: value}, {field: 0, [field.nestedField: 1, ...]})
// db.collectionName.find({field: value}, {field: 0, [field.nestedField: 0, ...]})

// MINI ACTIVITY:
// Retrieve all documents where age is greater than 75
// Include firstName, lastName, and phone number to the resulting documents
 db.users.find(
 		{"age": {$gt: 75}}, 
 		{"firstName": 1,
 		 "lastName": 1,
 		 "contact.phone": 1});

 // (Evaluation) Query Operator

 // $regex : patterns
 // SYNTAX:
 //  db.collection.find({field: $regex: 'pattern', $options: '$optionValue'});

// Case Sensitive
 db.users.find(
	{"firstName": {$regex: "N"}}
	);

// Case Insensitive
 db.users.find(
	{"firstName": {$regex: "n", $options: '$i'}}
	);

